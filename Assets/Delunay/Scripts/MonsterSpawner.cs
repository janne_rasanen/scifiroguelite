﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class MonsterSpawner : MonoBehaviour {
    private scController mapGenerator;

    public GameObject monsterContainer;
    private GameObject player;

    [System.Serializable]
    public class Spawn
    {
        public GameObject prefab;
        public int amountMin, amountMax;
    }

    [System.Serializable]
    public class SpawnGroup
    {
        public Spawn[] spawns;
        public float probability;
        [HideInInspector]
        public float chanceStart, chanceEnd;
        public string name; // just for nicer ui and debugging
    }

    public SpawnGroup[] possibleSpawns;

    void Start () {
        mapGenerator = GetComponent<Init>().mapGenerator;
        if (possibleSpawns != null)
        {
            float sum = possibleSpawns.Length > 0 ? possibleSpawns.Sum(s => s.probability) : 0;
            float lastChance = 0;
            foreach (var s in possibleSpawns)
            {
                s.chanceStart = lastChance;
                lastChance = s.chanceEnd = s.chanceStart + s.probability / sum;
                Debug.Log(s.name + ": " + s.chanceStart + " - " + s.chanceEnd);
            }
        }
	}
	
	public void SpawnStartingMonsters(scVertexNode playerRoom)
    {
        if (possibleSpawns == null || possibleSpawns.Length == 0)
        {
            Debug.LogWarning("No possible monsters in list to spawn");
            return;
        }

        SpawnMonsters(mapGenerator.roomList.Where(r => r != playerRoom).ToList());
    }

    private void SpawnMonsters(List<scVertexNode> toRooms)
    {
        for (int i = 0; i < toRooms.Count; i++)
        {
            scVertexNode room = toRooms[i];

            float roll = Random.Range(0f, 1f);
            SpawnGroup spawnGroup = possibleSpawns.FirstOrDefault(s => roll >= s.chanceStart && roll < s.chanceEnd);

            foreach (Spawn s in spawnGroup.spawns)
            {
                int amount = Random.Range(s.amountMin, s.amountMax);
                SpawnMonsters(s.prefab, room, amount);
            }
        }
    }

    public void SpawnReinforcementWave()
    {
        if (player == null)
            player = FindObjectOfType<PlayerMovement>().gameObject;

        List<scVertexNode> nearbyRooms = mapGenerator.getNearestRooms(player.transform.position, 2);

        SpawnMonsters(nearbyRooms);
    }

    private void SpawnMonsters(GameObject prefab, scVertexNode node, int amount)
    {
        while(amount-- > 0)
        {
            scRoom room = node.getParentCell().GetComponent<scRoom>();
            Vector3 pos = room.GetRandomPosWithinRoom(1f, 1f); // todo: read this from prefabs size
            pos.y = pos.y + 1;
            Quaternion rot = Quaternion.identity; // todo: randomize facing of monster
            Instantiate(prefab, pos, rot, monsterContainer.transform);            
        }
    }
}
