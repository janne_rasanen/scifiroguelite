// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32857,y:32871,varname:node_3138,prsc:2|emission-8559-RGB,clip-9245-OUT;n:type:ShaderForge.SFN_Tex2d,id:995,x:32465,y:32990,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_995,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:3808efc92f56bc74bb223741ec7a3f81,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:7562,x:32215,y:33021,ptovrint:False,ptlb:ShapeAnimation,ptin:_ShapeAnimation,varname:node_7562,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:37909a1974c3ee644996a07bf77d98d1,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:9245,x:32655,y:33150,varname:node_9245,prsc:2|A-995-R,B-5858-OUT;n:type:ShaderForge.SFN_Step,id:5858,x:32465,y:33174,varname:node_5858,prsc:2|A-7562-R,B-8247-OUT;n:type:ShaderForge.SFN_OneMinus,id:8247,x:32215,y:33202,varname:node_8247,prsc:2|IN-890-OUT;n:type:ShaderForge.SFN_VertexColor,id:8559,x:31753,y:32828,varname:node_8559,prsc:2;n:type:ShaderForge.SFN_Slider,id:6423,x:31593,y:33160,ptovrint:False,ptlb:node_6423,ptin:_node_6423,varname:node_6423,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3641313,max:1;n:type:ShaderForge.SFN_RemapRange,id:890,x:31984,y:33091,varname:node_890,prsc:2,frmn:0,frmx:1,tomn:0.4,tomx:2|IN-8559-A;proporder:995-7562-6423;pass:END;sub:END;*/

Shader "Shader Forge/ParticleAnimatedCutout" {
    Properties {
        _MainTex ("MainTex", 2D) = "bump" {}
        _ShapeAnimation ("ShapeAnimation", 2D) = "bump" {}
        _node_6423 ("node_6423", Range(0, 1)) = 0.3641313
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _ShapeAnimation; uniform float4 _ShapeAnimation_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 _MainTex_var = UnpackNormal(tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex)));
                float3 _ShapeAnimation_var = UnpackNormal(tex2D(_ShapeAnimation,TRANSFORM_TEX(i.uv0, _ShapeAnimation)));
                clip((_MainTex_var.r*step(_ShapeAnimation_var.r,(1.0 - (i.vertexColor.a*1.6+0.4)))) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = i.vertexColor.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _ShapeAnimation; uniform float4 _ShapeAnimation_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 _MainTex_var = UnpackNormal(tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex)));
                float3 _ShapeAnimation_var = UnpackNormal(tex2D(_ShapeAnimation,TRANSFORM_TEX(i.uv0, _ShapeAnimation)));
                clip((_MainTex_var.r*step(_ShapeAnimation_var.r,(1.0 - (i.vertexColor.a*1.6+0.4)))) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
