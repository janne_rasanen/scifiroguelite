﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class InstanceProgress : MonoBehaviour {
    public static InstanceProgress Instance;
    private Animator progressAnimator;

    private int enemies = 0;
    private Dictionary<LootType, int> loot = new Dictionary<LootType, int>();

    public int GetLoot(LootType forType)
    {
        return loot[forType];
    }

    private GameOverGlitch gameOverGlitch;

    const int wavesPerLevel = 5;
    private int currentWave = 1;
    private MonsterSpawner spawner;

    public Text waveText;

	void Awake () {
        Instance = this;
        progressAnimator = GetComponent<Animator>();
        gameOverGlitch = GetComponent<GameOverGlitch>();
        spawner = FindObjectOfType<MonsterSpawner>();
    }
	
    void Start()
    {
        ShowWaveProgress();
    }

	public void PlayerDied()
    {
        gameOverGlitch.enabled = true;
        progressAnimator.SetTrigger("GameOver");
    }

    public void EnemyDied()
    {
        enemies--;
        if (enemies <= 0)
        {
            currentWave++;
            if (currentWave > wavesPerLevel)
                LevelCompleted();
            else
            {
                ShowWaveProgress();
                spawner.SpawnReinforcementWave();
            }                
        }
    }

    public void EnemySpawned()
    {
        enemies++;
    }

    private void LevelCompleted()
    {
        CreateLoot();
        // todo: stop player input
        // todo: make player invincible
        progressAnimator.SetTrigger("LevelCompleted");
    }

    private void CreateLoot()
    {
        // todo: something more sophisticated ;)
        loot[LootType.Red] = Random.Range(10, 30);
        loot[LootType.Green] = Random.Range(10, 30);
        loot[LootType.Blue] = Random.Range(10, 30);

        Inventory.Instance.Add(LootType.Red, loot[LootType.Red]);
        Inventory.Instance.Add(LootType.Green, loot[LootType.Green]);
        Inventory.Instance.Add(LootType.Blue, loot[LootType.Blue]);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
            LevelCompleted();

        if (Input.GetKeyDown(KeyCode.P))
            PlayerDied();
    }

    public void OnRestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnMainMenu()
    {
        SceneManager.LoadScene("Scenes/MainMenu");
    }

    private void ShowWaveProgress()
    {
        if (waveText != null)
            waveText.text = "Wave " + currentWave + " / " + wavesPerLevel;
        progressAnimator.SetTrigger("WaveCompleted");
    }
}
