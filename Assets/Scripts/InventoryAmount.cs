﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryAmount : MonoBehaviour {
    public LootType type;

	void Start () {
        int amount = Inventory.Instance.GetAmount(type);
        GetComponent<Text>().text = amount.ToString();
	}
}
