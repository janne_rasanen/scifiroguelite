﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour {

    public Text infoTextCurrentLvl, infoTextNextLvl, lvlUpCostText;
    public Transform iconBg;
    public Animator animator;
    public Button lvlUpButton;

    private BoosterData booster;

	void Start () {
        animator = GetComponent<Animator>();
	}

    public void BoosterSelected(BoosterData booster)
    {
        animator.SetTrigger("BoosterSelected");
        bool firstTime = this.booster == null;
        this.booster = booster;

        if (firstTime)
        {
            ShowSelectedBoostersData();
        }
    }

    public void ShowSelectedBoostersData()
    {
        // shows the right icon, discard the old
        string prefabName = "boosterIcons/" + booster.iconPrefabName;
        for (int i = 0; i < iconBg.childCount; i++)
        {
            Destroy(iconBg.GetChild(i).gameObject);
        }
        Instantiate(Resources.Load(prefabName), iconBg, false);

        infoTextCurrentLvl.text = BoosterManager.Instance.GetDescText(booster.type, booster.level);
        if (BoosterManager.Instance.IsAtMaxLevel(booster.type))
            infoTextNextLvl.text = "";
        else
            infoTextNextLvl.text = BoosterManager.Instance.GetDescText(booster.type, booster.level + 1);
        lvlUpCostText.text = BoosterManager.Instance.GetCostText(booster.type);

        lvlUpButton.interactable = PlayerCanAfford();
    }

    private bool PlayerCanAfford()
    {
        return Inventory.Instance.GetAmount(booster.lootRequiredToLevel) >= BoosterManager.Instance.GetNextLevelCost(booster.type);
    }

    public void OnUpgradeCurrentBooster()
    {
        if (!PlayerCanAfford())
        {
            Debug.LogError("Upgrade button pressed but player can't afford to upgrade");
            return;
        }

        int cost = BoosterManager.Instance.GetNextLevelCost(booster.type);

        BoosterManager.Instance.Upgrade(booster.type);
        Inventory.Instance.Add(booster.lootRequiredToLevel, -cost);

        animator.SetTrigger("LvlUp");
    }
}
