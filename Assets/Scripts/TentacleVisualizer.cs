﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TentacleVisualizer : MonoBehaviour {
    public Transform[] points;

    public int segmentsInLineRenderer = 10;

    private LineRendererSmoother smoother;
    private BezierCurve curve;

    private Vector3[] positions;

    void Awake()
    {
        smoother = GetComponent<LineRendererSmoother>();
        curve = GetComponent<BezierCurve>();
    }

	void Start () {
        smoother.Init(GetWorldPos());
    }
	
	void Update () {
        smoother.SetTargets(GetWorldPos());
	}

    private Vector3[] GetWorldPos()
    {
        if (positions == null || positions.Length != segmentsInLineRenderer)
        {
            positions = new Vector3[segmentsInLineRenderer];
        }    

        float step = 1f / (segmentsInLineRenderer - 1);

        for(int i=0; i < segmentsInLineRenderer; i++)
        {
            positions[i] = curve.GetPointAt(i * step);
        }

        return positions;
    }
}
