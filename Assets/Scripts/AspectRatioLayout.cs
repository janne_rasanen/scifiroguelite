﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AspectRatioLayout : MonoBehaviour {

    private LayoutElement layout;
    private RectTransform rect;
	void Start () {
        layout = GetComponent<LayoutElement>();
        rect = GetComponent<RectTransform>();
	}
	
	void Update () {
        if (layout.preferredHeight != rect.rect.width || rect.hasChanged)
        {
            rect.hasChanged = false;
            layout.preferredHeight = rect.rect.width;
            LayoutRebuilder.MarkLayoutForRebuild(rect);
        }
	}
}
