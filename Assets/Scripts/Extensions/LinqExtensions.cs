﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class LinqExtensions {

    public static T RandomOrDefault<T>(this IEnumerable<T> enumerable)
    {
        if (enumerable.Count() == 0)
            return default(T);
        int index = Random.Range(0, enumerable.Count());
        return enumerable.ElementAt(index);
    }
}
