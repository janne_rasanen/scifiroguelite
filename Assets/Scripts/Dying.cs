﻿using UnityEngine;
using System.Collections;

public class Dying : MonoBehaviour {
    public void OnDyingDone()
    {
        Destroy(gameObject);
    }
}
