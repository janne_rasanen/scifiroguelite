﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LootAmount : MonoBehaviour {
    public LootType lootType;

    void Start () {
        InstanceProgress progress = GetComponentInParent<InstanceProgress>();
        int amount = progress.GetLoot(lootType);
        GetComponent<Text>().text = "+" + amount.ToString();
	}
}
