﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class BoosterListPopulator : MonoBehaviour {

    public GameObject prefab;

	void Start () {
        UpgradeMenu menu = GetComponentInParent<UpgradeMenu>();

        foreach (var bd in BoosterManager.Instance.boosters)
        {
            GameObject go = (GameObject)Instantiate(prefab, transform, false);

            string prefabName = "boosterIcons/" + bd.iconPrefabName;

            Instantiate(Resources.Load(prefabName), go.transform);

            BoosterData d = bd;

            go.GetComponent<Button>().onClick.AddListener(() =>
            {
                menu.BoosterSelected(d);
            });
        }
    }
}
