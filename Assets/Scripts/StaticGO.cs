﻿using UnityEngine;
using System.Collections;

public class StaticGO : MonoBehaviour {
    public static GameObject Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameObject("Static container");
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    private static GameObject _instance;
}
