﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 3f;

    private Vector3 tmp = new Vector3();
    private CharacterController controller;
    public Transform levelMovementDirections;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        tmp.Set(Input.GetAxis("LeftStickVertical"), 0, Input.GetAxis("LeftStickHorizontal"));
        tmp = levelMovementDirections.TransformDirection(tmp);
        tmp *= speed;
        controller.SimpleMove(tmp);
    }
}