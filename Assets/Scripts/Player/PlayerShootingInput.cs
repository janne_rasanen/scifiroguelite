﻿using UnityEngine;
using System.Collections;

public class PlayerShootingInput : MonoBehaviour {

    private Weapon[] weapons;

    private bool triggerDown = false;
    private Weapon activeWeapon;

    void Start () {
        weapons = GetComponentsInChildren<Weapon>();
        activeWeapon = weapons[0];
        activeWeapon.TriggerLifted();
	}
	
	void Update () {
        float f = Input.GetAxis("Fire1");

        if (triggerDown && f <= 0)
        {
            triggerDown = false;
            activeWeapon.TriggerLifted();
        }
            
        else if (!triggerDown && f > 0)
        {
            triggerDown = true;
            activeWeapon.TriggerPulled();
        }
    }
}
