﻿using UnityEngine;
using System.Collections;

public class PlayerAimingStick : MonoBehaviour {

    public float rotateSpeed = 6f;
    private Vector3 tmp = new Vector3();
    public Transform levelMovementDirections;
    private Quaternion latestTarget;

    void Start()
    {
        latestTarget = transform.rotation;
    }

    void Update()
    {
        tmp.Set(Input.GetAxis("RightStickVertical"), 0, Input.GetAxis("RightStickHorizontal"));
        tmp = levelMovementDirections.TransformDirection(tmp);
        if (tmp.sqrMagnitude > 0)
        {
            latestTarget = Quaternion.LookRotation(tmp, Vector3.up);
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, latestTarget, rotateSpeed * Time.deltaTime);
    }
}
