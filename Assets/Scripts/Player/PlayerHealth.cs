﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {
    public float maxHealth = 100f;
    private float currentHealth;
    private GlitchEffect glitchEffect;

    const float chancePerSec = 1 / 10f;

    void Awake()
    {
        currentHealth = maxHealth;
        glitchEffect = FindObjectOfType<GlitchEffect>();
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;

        Debug.Log(currentHealth);

        glitchEffect.Glitch(1f - currentHealth / maxHealth);

        if (currentHealth <= 0)
        {
            InstanceProgress.Instance.PlayerDied();
        }
    }

    void Update()
    {
        float ratio = currentHealth / maxHealth;
        if (ratio > 0.5f)
            return;

        float chance = chancePerSec * Time.deltaTime;

        if (ratio < 0.1f)
            chance *= 5;
        else if (ratio < 0.2f)
            chance *= 4;
        else if (ratio < 0.3f)
            chance *= 3;
        else if (ratio < 0.4f)
            chance *= 2;

        if (Random.Range(0f, 1f) < chance)
            glitchEffect.Glitch(1f - currentHealth / maxHealth);
    }
}