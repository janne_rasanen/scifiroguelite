﻿using UnityEngine;
using System.Collections;
using System;

public class ProjectileWeapon : MonoBehaviour, Weapon
{
    private ParticleSystem system;

    void Awake()
    {
        system = GetComponent<ParticleSystem>();
    }

    public void TriggerLifted()
    {
        system.Stop();
    }

    public void TriggerPulled()
    {
        system.Play();
    }

    void OnParticleCollision(GameObject other)
    {
        EnemyHealth target = other.GetComponentInChildren<EnemyHealth>();
        if (target == null)
            return;

        target.TakeDamage();
    }
}
