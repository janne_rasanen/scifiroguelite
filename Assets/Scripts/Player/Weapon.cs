﻿using UnityEngine;
using System.Collections;

public interface Weapon {

    void TriggerPulled();
    void TriggerLifted();
}
