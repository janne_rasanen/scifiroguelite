﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverGlitch : MonoBehaviour {

    private GlitchEffect effect;
    const float animTime = 3f;
    const float minGlitch = 1f, maxGlitch = 3f;
    private float startTime;

    void Awake () {
        effect = FindObjectOfType<GlitchEffect>();
	}

    void Start()
    {
        enabled = false;
    }
	
    void OnEnable()
    {
        startTime = Time.time;
    }

	void Update () {

        float progress = (Time.time - startTime) / animTime;
        float clamped = Mathf.Clamp01(progress);
        float intensity = minGlitch + clamped * (maxGlitch - minGlitch);
        effect.Glitch(intensity);
	}
}
