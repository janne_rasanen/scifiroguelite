﻿using UnityEngine;
using System.Collections;

public class PlayerAimingMouse : MonoBehaviour {
    private Camera cam;
    private RaycastHit hitInfo;
    public LayerMask mouseHitLayer;
    private Quaternion latestTarget;
    public float rotateSpeed = 6f;

    void Awake () {
        cam = Camera.main;
	}

    void Start()
    {
        latestTarget = transform.rotation;
    }
	
	void Update () {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        
        bool b = Physics.Raycast(ray, out hitInfo, Mathf.Infinity, mouseHitLayer);
        if (b)
        {
            latestTarget = Quaternion.LookRotation(hitInfo.point - transform.position, Vector3.up);
        }

        
        transform.rotation = Quaternion.Slerp(transform.rotation, latestTarget, rotateSpeed * Time.deltaTime);
    }
}
