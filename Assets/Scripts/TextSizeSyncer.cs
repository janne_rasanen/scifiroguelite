﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class TextSizeSyncer : MonoBehaviour {
    public Text[] minimums;

    private int framesToSkip = 1;

    void Update()
    {
        if (framesToSkip-- > 0)
            return;

        int min = minimums.Min(t => t.cachedTextGenerator.fontSizeUsedForBestFit);
        
        foreach(Text text in minimums)
        {
            text.fontSize = min;
            text.resizeTextForBestFit = false;
        }

        enabled = false;
    }
}
