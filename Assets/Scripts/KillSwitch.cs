﻿using UnityEngine;
using System.Collections;

public class KillSwitch : MonoBehaviour {
    public Animator[] cubeAnimators;

    private int currentAcIndex = 0;

    void Update () {
	    if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentAcIndex < cubeAnimators.Length)
                cubeAnimators[currentAcIndex++].SetTrigger("Die");
        }
	}
}
