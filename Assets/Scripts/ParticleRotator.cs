﻿using UnityEngine;
using System.Collections;

public class ParticleRotator : MonoBehaviour {

    private ParticleSystem system;

    void Awake()
    {
        system = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        // start the rotation of the projectile correctly based on current rotation
        system.startRotation3D = transform.rotation.eulerAngles * Mathf.Deg2Rad;
    }
}
