﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public void OnEnterLevel()
    {
        SceneManager.LoadScene("Scenes/GenerateDelayney");
    }

    public void OnUpgrade()
    {
        SceneManager.LoadScene("Scenes/Upgrade");
    }

    public void OnArmoury()
    {
        SceneManager.LoadScene("Scenes/Armoury");
    }
}