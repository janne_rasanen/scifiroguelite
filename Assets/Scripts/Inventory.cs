﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {
    public static Inventory Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = StaticGO.Instance;
                _instance = go.AddComponent<Inventory>();
            }
            return _instance;
        }
    }

    private static Inventory _instance;

    private Dictionary<string, int> inventory = new Dictionary<string, int>();

    void Awake () {
        LoadInventory();
	}

    public void Add(LootType type, int change)
    {
        Add(type.ToString(), change);
    }

    public void SetLevel(BoosterType type, int level)
    {
        Set(type.ToString(), level);
    }

    public int GetAmount(LootType type)
    {
        return GetAmount(type.ToString());
    }

    public int GetBoosterLevel(BoosterType type)
    {
        return GetAmount(type.ToString());
    }

    private int GetAmount(string name)
    {
        int amount;
        bool found = inventory.TryGetValue(name, out amount);
        if (!found)
            amount = 0;
        return amount;
    }

    private void Add(string name, int change)
    {
        int oldAmount = GetAmount(name);
        inventory[name] = oldAmount + change;
    }

    private void Set(string name, int newValue)
    {
        inventory[name] = newValue;
    }

    const string filename = "inventory.dat";

    private void LoadInventory()
    {
        inventory = DataStore.Load(filename);
    }

    void OnDestroy()
    {
        DataStore.Save(inventory, filename);
    }
}
