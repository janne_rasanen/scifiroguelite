﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TentacleSpawner : MonoBehaviour {
    public GameObject prefab;

	void Start () {
        GameObject o = (GameObject) Instantiate(prefab, transform.position, Quaternion.identity);
        Rigidbody r = GetComponent<Rigidbody>();
        foreach (CharacterJoint j in o
            .GetComponentsInChildren<CharacterJoint>()
            .Where(j => j.connectedBody == null))
            j.connectedBody = r;
            
	}
}
