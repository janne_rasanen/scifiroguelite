﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class DataStore {

    public static Dictionary<string, int> Load(string filename)
    {
        try
        {
            using (var stream = new FileStream(GetPath(filename), FileMode.Open, FileAccess.Read))
            {
                BinaryReader reader = new BinaryReader(stream);
                int count = reader.ReadInt32();
                var dictionary = new Dictionary<string, int>(count);
                for (int n = 0; n < count; n++)
                {
                    var key = reader.ReadString();
                    var value = reader.ReadInt32();
                    dictionary.Add(key, value);
                }
                return dictionary;
            }
        }
        catch(FileNotFoundException e)
        {
            Debug.LogWarning("Couldn't find file " + e.ToString());
            return new Dictionary<string, int>();
        }
    }

    public static void Save(Dictionary<string, int> dic, string filename)
    {
        try
        {
            using (var stream = new FileStream(GetPath(filename), FileMode.Create))
            {
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(dic.Count);
                foreach (var kvp in dic)
                {
                    writer.Write(kvp.Key);
                    writer.Write(kvp.Value);
                }
                writer.Flush();
            }
        }
        catch(FileNotFoundException e)
        {
            Debug.LogError("Couldn't save to file: " + e.ToString());
        }
        catch (System.UnauthorizedAccessException e)
        {
            Debug.LogError("Unauthorized when trying to save to file: " + e.ToString());
        }
    }

    private static string GetPath(string filename)
    {
        return Application.persistentDataPath + "/" + filename;
    }
}
