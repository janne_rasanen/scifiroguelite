﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class LineRendererSmoother : MonoBehaviour {
    public float velocity = 1f;

    private LineRenderer lr;
    private Vector3[] targetPos;
    private Vector3[] posLastFrame;

	void Awake () {
        lr = GetComponent<LineRenderer>();
	}
	
    public void SetTargets(Vector3[] targetPos)
    {
        if (this.targetPos == null || this.targetPos.Length != targetPos.Length)
        {
            Init(targetPos);
        }
        else
            this.targetPos = targetPos;
    }

    public void Init(Vector3[] pos)
    {
        lr.SetVertexCount(pos.Length);
        lr.SetPositions(pos);
        targetPos = posLastFrame = pos;
    }

	void LateUpdate () {
	    for (int i=0; i < targetPos.Length; i++)
        {
            posLastFrame[i] = Vector3.MoveTowards(posLastFrame[i], targetPos[i], velocity * Time.deltaTime);
        }
        lr.SetPositions(posLastFrame);
	}

    public float GetTotalDst()
    {
        if (posLastFrame == null || posLastFrame.Length == 0)
            return 0;

        return posLastFrame
            .Select((lastPos, index) => Vector3.Distance(lastPos, targetPos[index]))
            .Sum();        
    }
}
