﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {
    private ParticleSystem weaponPS;
    public float damage = 1f;
	
    void Start()
    {
        weaponPS = GetComponent<ParticleSystem>();
    }

	public void ShootIfPossible(Vector3 target)
    {
        if (weaponPS.isPlaying)
            return;

        // since the ai already rotates the enemy towards the player, no need to rotate here
        // transform.LookAt(target);
        // todo: aiming fluctuation here
        weaponPS.Play();
    }

    void OnParticleCollision(GameObject other)
    {
        PlayerHealth target = other.GetComponentInParent<PlayerHealth>();
        if (target == null)
            return;

        target.TakeDamage(damage);   
    }
}
