﻿using UnityEngine;
using System.Collections;

public class RandomRotationMultiplier : MonoBehaviour {
    public AnimationCurve speedCurve;

    private float personalSpeedMultiplier;
	void Start () {
        personalSpeedMultiplier = Random.Range(-1f, 1f);
	}

    void Update()
    {
        GetComponent<Animator>().SetFloat("SpeedMultiplier", personalSpeedMultiplier * speedCurve.Evaluate(Time.time));
    }
}
