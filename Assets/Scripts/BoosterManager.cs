﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class BoosterManager : MonoBehaviour {
    public static BoosterManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = StaticGO.Instance;
                _instance = go.AddComponent<BoosterManager>();
            }
            return _instance;
        }
    }

    private static BoosterManager _instance;

    private Dictionary<BoosterType, BoosterLevelInfo[]> effects;

    private class BoosterLevelInfo
    {
        public int lootRequired;
        public float[] effects;
    }

    void Awake() {
        LoadEffectData();
        LoadBoosters();
	}

    private void LoadEffectData()
    {
        effects = new Dictionary<BoosterType, BoosterLevelInfo[]>();
        effects[BoosterType.Split] = new BoosterLevelInfo[] {
            new BoosterLevelInfo { lootRequired = 0, effects = new float[] { 3, 0.4f }},
            new BoosterLevelInfo { lootRequired = 1, effects = new float[] { 3, 0.45f }},
            new BoosterLevelInfo { lootRequired = 2, effects = new float[] { 4, 0.375f }},
            new BoosterLevelInfo { lootRequired = 3, effects = new float[] { 4, 0.4125f }},
            new BoosterLevelInfo { lootRequired = 4, effects = new float[] { 5, 0.34f }},
        };

        effects[BoosterType.Seeking] = new BoosterLevelInfo[] {
            new BoosterLevelInfo { lootRequired = 0, effects = new float[] { 0.1f }},
            new BoosterLevelInfo { lootRequired = 100, effects = new float[] { 0.2f }},
            new BoosterLevelInfo { lootRequired = 200, effects = new float[] { 0.3f }},
            new BoosterLevelInfo { lootRequired = 300, effects = new float[] { 0.4f }},
            new BoosterLevelInfo { lootRequired = 400, effects = new float[] { 0.5f }},
        };

        effects[BoosterType.Vampire] = new BoosterLevelInfo[] {
            new BoosterLevelInfo { lootRequired = 0, effects = new float[] { 10f }},
            new BoosterLevelInfo { lootRequired = 100, effects = new float[] { 12f }},
            new BoosterLevelInfo { lootRequired = 200, effects = new float[] { 14f }},
            new BoosterLevelInfo { lootRequired = 300, effects = new float[] { 16f }},
            new BoosterLevelInfo { lootRequired = 400, effects = new float[] { 18f }},
        };

        effects[BoosterType.Haste] = new BoosterLevelInfo[] {
            new BoosterLevelInfo { lootRequired = 0, effects = new float[] { 0.1f }},
            new BoosterLevelInfo { lootRequired = 100, effects = new float[] { 0.2f }},
            new BoosterLevelInfo { lootRequired = 200, effects = new float[] { 0.3f }},
            new BoosterLevelInfo { lootRequired = 300, effects = new float[] { 0.4f }},
            new BoosterLevelInfo { lootRequired = 400, effects = new float[] { 0.5f }},
        };

        effects[BoosterType.Damage] = new BoosterLevelInfo[] {
            new BoosterLevelInfo { lootRequired = 0, effects = new float[] { 0.1f }},
            new BoosterLevelInfo { lootRequired = 100, effects = new float[] { 0.2f }},
            new BoosterLevelInfo { lootRequired = 200, effects = new float[] { 0.3f }},
            new BoosterLevelInfo { lootRequired = 300, effects = new float[] { 0.4f }},
            new BoosterLevelInfo { lootRequired = 400, effects = new float[] { 0.5f }},
        };
    }

    public List<BoosterData> boosters { get; private set; }
	
	private void LoadBoosters()
    {
        boosters = new List<BoosterData>(new BoosterData[] {
            new BoosterData { type = BoosterType.Split, lootRequiredToLevel = LootType.Blue, name = "Split", level = 0, iconPrefabName = "cubes" },
            new BoosterData { type = BoosterType.Seeking, lootRequiredToLevel = LootType.Green, name = "Seeking", level = 0, iconPrefabName = "leo" },
            new BoosterData { type = BoosterType.Vampire, lootRequiredToLevel = LootType.Green,name = "Vampire", level = 0, iconPrefabName = "totem-head" },
            new BoosterData { type = BoosterType.Haste, lootRequiredToLevel = LootType.Blue, name = "Haste", level = 0, iconPrefabName = "aquarius" },
            new BoosterData { type = BoosterType.Damage, lootRequiredToLevel = LootType.Red,name = "Damage", level = 0, iconPrefabName = "doubled" },
        });

        foreach(var b in boosters)
        {
            b.level = Inventory.Instance.GetBoosterLevel(b.type);
        }
    }

    public string GetDescText(BoosterType type, int level)
    {
        string result = "Level {0}\n";
        switch(type)
        {
            case BoosterType.Split:
                result += "Split to {1} projectiles each doing {2}% damage of the original projectile"; break;
            case BoosterType.Seeking:
                result += "Seeking projectiles with {1}% lateral velocity"; break;
            case BoosterType.Vampire:
                result += "Gain {1} health per enemy hit"; break;
            case BoosterType.Haste:
                result += "{1}% faster projectiles"; break;
            case BoosterType.Damage:
                result += "{1}% extra damage"; break;
            default:
                result = "NA"; break;

        }

        result = string.Format(result, GetEffectFormat(type, level));

        return result;
    }

    // returns an array that has
    // [0] = level.ToString()
    // and rest of indices are the effects of the booster on that level
    private object[] GetEffectFormat(BoosterType type, int level)
    {
        BoosterLevelInfo lvlInfo = effects[type][level];

        string[] result = new string[] { level.ToString() };
        IEnumerable<string> e = lvlInfo.effects.Select(f => f.ToString("0.00"));
        result = result.Concat(e).ToArray();
        return result;
    }

    public string GetCostText(BoosterType type)
    {
        BoosterData data = boosters.Single(b => b.type == type);

        BoosterLevelInfo[] lvlInfos = effects[type];

        if (data.level + 1 >= lvlInfos.Length)
            return "Max lvl reached";

        string result = "Lvl up\n";
        result += "Costs <color=";

        switch (data.lootRequiredToLevel)
        {
            case LootType.Blue:
                result += "blue";
                break;
            case LootType.Red:
                result += "red";
                break;
            case LootType.Green:
                result += "green";
                break;
        }
        result += ">";

        result += lvlInfos[data.level + 1].lootRequired;

        result += "</color>";

        return result;
    }

    public int GetNextLevelCost(BoosterType type)
    {
        BoosterData data = boosters.Single(b => b.type == type);

        BoosterLevelInfo[] lvlInfos = effects[type];

        if (data.level + 1 >= lvlInfos.Length)
            return int.MaxValue;

        return lvlInfos[data.level + 1].lootRequired;
    }

    public void Upgrade(BoosterType type)
    {
        BoosterData data = boosters.Single(b => b.type == type);
        data.level++;
        Inventory.Instance.SetLevel(data.type, data.level);
    }

    public bool IsAtMaxLevel(BoosterType type)
    {
        BoosterData data = boosters.Single(b => b.type == type);

        BoosterLevelInfo[] lvlInfos = effects[type];

        return data.level + 1 >= lvlInfos.Length;
    }
}
