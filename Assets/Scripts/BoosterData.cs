﻿using UnityEngine;
using System.Collections;

public class BoosterData {

    public int level;

    public string iconPrefabName;

    public string name;

    public BoosterType type;

    public LootType lootRequiredToLevel;
}
