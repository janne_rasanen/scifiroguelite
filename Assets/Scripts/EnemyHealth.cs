﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
    public Animator[] cubeAnimators;
    public Animator finalDeathAc;
    public GameObject root;
    public ParticleSystem[] stopOnZeroHp;

    private int currentAcIndex = 0;

    void Start()
    {
        InstanceProgress.Instance.EnemySpawned();
    }

    public void TakeDamage()
    {
        if (currentAcIndex < cubeAnimators.Length)
            cubeAnimators[currentAcIndex++].SetTrigger("Die");

        if (currentAcIndex >= cubeAnimators.Length)
        {
            finalDeathAc.SetTrigger("Die");
            for (int i = 0; i < stopOnZeroHp.Length; i++)
                stopOnZeroHp[i].Stop();
            InstanceProgress.Instance.EnemyDied();
        }
            
    }
}