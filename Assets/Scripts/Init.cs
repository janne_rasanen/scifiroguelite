﻿using UnityEngine;
using System.Collections;
using System;
using RAIN.Navigation.NavMesh;
using System.Collections.Generic;
using System.Linq;

public class Init : MonoBehaviour {
    public scController mapGenerator;
    public GameObject playerPrefab;
    public Transform levelMovementDirections;
    public NavMeshRig navMeshRig;

    private MonsterSpawner monsterSpawner;
    const float printDebugStep = 0.1f;
    private float lastProgress = 0f;
    public bool mouse = true;

    void Start () {
        mapGenerator.onMapCreated = OnMapCreated;
        monsterSpawner = GetComponent<MonsterSpawner>();
	}
	
	private void OnMapCreated()
    {
        StartCoroutine(OnMapCreatedRoutine());
    }

    private IEnumerator OnMapCreatedRoutine()
    {
        yield return StartCoroutine(CreateNavMesh());
        scVertexNode playerRoom = SpawnPlayer();
        monsterSpawner.SpawnStartingMonsters(playerRoom);
    }

    private scVertexNode SpawnPlayer()
    {
        scVertexNode randomRoom = GetStartingRoom();
        Vector2 roomPos = randomRoom.getVertexPosition();
        GameObject go = Instantiate(playerPrefab);
        go.transform.position = new Vector3(roomPos.x, 3, roomPos.y);
        go.GetComponentInChildren<PlayerMovement>().levelMovementDirections = levelMovementDirections;
        go.GetComponentInChildren<PlayerAimingStick>().levelMovementDirections = levelMovementDirections;

        if (mouse)
            go.GetComponent<PlayerAimingStick>().enabled = false;
        else
            go.GetComponent<PlayerAimingMouse>().enabled = false;

        return randomRoom;
    }

    private scVertexNode GetStartingRoom()
    {
        scVertexNode hasAtLeastTwoExits = mapGenerator.GetRandomRoomWithExits(2);
        if (hasAtLeastTwoExits != null)
            return hasAtLeastTwoExits;
        else
            return mapGenerator.GetRandomRoom();
    }

    private IEnumerator CreateNavMesh()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("NavMesh generation starting");
        navMeshRig.NavMesh.UnregisterNavigationGraph();

        navMeshRig.NavMesh.StartCreatingContours(4);
        while (navMeshRig.NavMesh.Creating)
        {
            navMeshRig.NavMesh.CreateContours();

            yield return new WaitForEndOfFrame();

            while (navMeshRig.NavMesh.CreatingProgress >= lastProgress + printDebugStep)
            {
                lastProgress += printDebugStep;
                Debug.Log("NavMesh generation " + lastProgress);
            }
        }

        navMeshRig.NavMesh.RegisterNavigationGraph();
        Debug.Log("NavMesh generation finished");
    }
}
