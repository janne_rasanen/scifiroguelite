﻿using UnityEngine;
using System.Collections;

public class CameraUpdater : MonoBehaviour {
    public Vector3 camOffset;

    private Transform cam;
    private Vector3 offset;
    private bool needsLookUpdate = true;

	void Start () {
        cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }
	
	void Update () {
        cam.position = transform.position + camOffset;
        if (needsLookUpdate)
        {
            cam.LookAt(transform);
            needsLookUpdate = false;
        }
    }
}
