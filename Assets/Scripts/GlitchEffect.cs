﻿using UnityEngine;

public class GlitchEffect : MonoBehaviour
{
    public Texture2D displacementMap;
    float glitchup, glitchdown, flicker,
            glitchupTime = 0.05f, glitchdownTime = 0.05f, flickerTime = 0.05f;

    private float intensity;
    private Material material;

    private int counterFlicker = 0, counterGlitchUp = 0, counterGlitchDown = 0, counterDisplace = 0;

    void Awake()
    {
        material = new Material(Shader.Find("Hidden/GlitchShader"));
        material.SetTexture("_DispTex", displacementMap);
        material.SetFloat("flip_up", 0);
        material.SetFloat("flip_down", 1);
        material.SetFloat("filterRadius", 0f);
        material.SetFloat("displace", 0f);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            Glitch(0.25f);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            Glitch(0.6f);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            Glitch(0.8f);
        }
    }

    public void Glitch(float intensity)
    {
        this.intensity = intensity;
        material.SetFloat("_Intensity", intensity);

        int max;
        if (intensity > 0.75)
            max = 3;
        else if (intensity > 0.5)
            max = 2;
        else
            max = 1;

        counterFlicker += Random.Range(0, max + 1);
        counterGlitchUp += Random.Range(0, max + 1);
        counterGlitchDown += Random.Range(0, max + 1);
        counterDisplace += Random.Range(0, max * 3 + 1);
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        glitchup += Time.deltaTime * intensity;
        glitchdown += Time.deltaTime * intensity;
        flicker += Time.deltaTime * intensity;

        if (flicker > flickerTime)
        {
            float flickerValue = counterFlicker > 0 ? Random.Range(-3f, 3f) * intensity : 0;
            counterFlicker--;
            material.SetFloat("filterRadius", flickerValue);
            
            flicker = 0;
            flickerTime = Random.value;
        }
        
        if (glitchup > glitchupTime)
        {
            float flipUpValue = counterGlitchUp > 0 ? Random.Range(0, 1f) * intensity : 0;
            counterGlitchUp--;
            material.SetFloat("flip_up", flipUpValue);

            glitchup = 0;
            glitchupTime = Random.value / 10f;
        }

        
        if (glitchdown > glitchdownTime)
        {
            float flipDownValue = counterGlitchDown > 0 ? 1 - Random.Range(0, 1f) * intensity : 1;
            counterGlitchDown--;

            material.SetFloat("flip_down", flipDownValue);

            glitchdown = 0;
            glitchdownTime = Random.value / 10f;
        }
        
        if (/* Random.value < 0.05 * intensity && */ counterDisplace > 0)
        {
            counterDisplace--;
            material.SetFloat("displace", Random.value * intensity);
            material.SetFloat("scale", 1 - Random.value * intensity);
        }
        else
            material.SetFloat("displace", 0f);
        

        Graphics.Blit(source, destination, material);
    }
}