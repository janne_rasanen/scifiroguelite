using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Entities.Aspects;

[RAINAction]
public class EnemyShootingAction : RAINAction
{
    private EnemyShooting shooting;

    public override void Start(AI ai)
    {
        base.Start(ai);
        shooting = ai.Body.GetComponentInChildren<EnemyShooting>();
    }

    public override ActionResult Execute(AI ai)
    {
        VisualAspect aspect = ai.WorkingMemory.GetItem<VisualAspect>("player");
        if (aspect != null)
        {
            shooting.ShootIfPossible(aspect.Position);
        }
            
        return ActionResult.RUNNING;
    }

    public override void Stop(AI ai)
    {
        base.Stop(ai);
    }
}